//
//  AttributedStringAppDelegate.h
//  AttributedString
//
//  Created by Bharath Krishnan on 2/15/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttributedStringAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
