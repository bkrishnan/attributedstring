//
//  main.m
//  AttributedString
//
//  Created by Bharath Krishnan on 2/15/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AttributedStringAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AttributedStringAppDelegate class]));
    }
}
