//
//  AttributedStringViewController.m
//  AttributedString
//
//  Created by Bharath Krishnan on 2/15/13.
//  Copyright (c) 2013 MyOrg. All rights reserved.
//

#import "AttributedStringViewController.h"

@interface AttributedStringViewController ()
@property (weak, nonatomic) IBOutlet UILabel *selectedWordLabel;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIStepper *wordStepper;

@end

@implementation AttributedStringViewController

- (void) addLabelAttributes:(NSDictionary *)attributes forRange:(NSRange)range
{
    if (range.location != NSNotFound) {
        NSMutableAttributedString *mat = [self.label.attributedText mutableCopy];
        [mat addAttributes:attributes range:range];
        self.label.attributedText = mat;
    }
}

- (void) addSelectedWordAttributes:(NSDictionary *)attributes
{
    NSRange range = [[self.label.attributedText string] rangeOfString:[self selectedWord]];
    [self addLabelAttributes:attributes forRange:range];
    
}
- (IBAction)underline {
    [self addSelectedWordAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
}

- (IBAction)ununderline:(id)sender {
    [self addSelectedWordAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}];
}
- (IBAction)changeColor:(UIButton *)sender {
    [self addSelectedWordAttributes:@{NSForegroundColorAttributeName: sender.backgroundColor}];
}

- (IBAction)changeFont:(UIButton *)sender {
    CGFloat fontSize = [UIFont systemFontSize];
    NSDictionary *attr = [self.label.attributedText attributesAtIndex:0 effectiveRange:nil];
    UIFont *existingFont = attr[NSFontAttributeName];
    if (existingFont) fontSize = existingFont.pointSize;
    
    UIFont *newFont = [sender.titleLabel.font fontWithSize:fontSize];
    [self addSelectedWordAttributes:@{NSFontAttributeName : newFont}];
    
}

- (NSArray *) wordList
{
    NSArray * wordList = [[self.label.attributedText string] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (![wordList count]) {
        wordList = @[@""];
    }
    
    return wordList;
}

- (NSString *) selectedWord {
    return [[self wordList] objectAtIndex:(int)self.wordStepper.value];
    
    
}

- (IBAction)updateSelectedWord {
    self.wordStepper.maximumValue = ([[self wordList] count] - 1);
    self.selectedWordLabel.text = [self selectedWord];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateSelectedWord];
}

@end
